var v_taskPanel = new Vue({
    el: '#v_taskPanel',
    data: {
        panelList:
            [
                {
                    title: "名前の登録",
                    description: "会員登録メニューで名前を登録しましょう",
                    hintList: [
                        {
                            title: "参考ページ",
                            link: "http://google.com",
                        }
                    ],
                    buttonList : [
                        {"title" : "開始"},
                        {"title" : "終了"}
                    ]
                }
            ]
    },
    methods: {
        viewOpen: function () {
            // $(".menu").click(function() {  //メニューの開閉ファンクション
            $("#v_menu").show();
            // });
        }
    }
})

