package com.sk.tutorialService.controller.view;

import com.sk.tutorialService.annotation.LoginSession;
import com.sk.tutorialService.bean.session.LoginSessionData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Slf4j
@Controller
@RequestMapping("/tutorial")
public class TutorialController {

        private static String BASE_VIEW_PATH = "views/tutorial/";

        @RequestMapping("/")
        public String index() {
                return BASE_VIEW_PATH + "index";
        }

        @RequestMapping(method = RequestMethod.GET, path = "/{fileName}")
        public String mainGet(ModelMap modelMap, @PathVariable("fileName") String fileName) {

                String templatePath = BASE_VIEW_PATH + fileName;
                return templatePath;
        }

        @RequestMapping(method = RequestMethod.GET, path = "_/{fileName}")
        public String mainGetWIthSession(ModelMap modelMap, @PathVariable("fileName") String fileName,
                @LoginSession LoginSessionData loginSessionData) {
                checkAndSetLoginSessionData(loginSessionData);

                String templatePath = BASE_VIEW_PATH + fileName;
                return templatePath;
        }

        private void checkAndSetLoginSessionData(LoginSessionData loginSessionData) {
                if (loginSessionData == null) {
                        log.error("セッション情報なし");
                } else {
                        log.info(loginSessionData.toString());
                }
        }
}
