package com.sk.tutorialService.db.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "CODE_MANAGE", catalog = "PUBLIC")
public class CodeManage extends CommonEntity{

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;

        @Column(name = "CODE_NAME")
        private String codeName;

        /**
         *
         */
        @Column(name = "CODE_PREFIX")
        private String codePrefix;

        /**
         *
         */
        @ColumnDefault(value = "0")
        @Column(name = "CURRENT_CODE_NUMBER_00")
        private Long currentCodeNumber00;

        @ColumnDefault(value = "0")
        @Column(name = "CURRENT_CODE_NUMBER_01")
        private Long currentCodeNumber01;

        @ColumnDefault(value = "10")
        @Column(name = "INCREMENT_00")
        private Long increment_00;

        @ColumnDefault(value = "10")
        @Column(name = "INCREMENT_01")
        private Long increment_01;

}
