package com.sk.tutorialService.db.repository;

import com.sk.tutorialService.db.entity.LoginUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface LoginUserRepository extends JpaRepository<LoginUser, Long> {

    void removeByLoginName(@Param("loginName") String loginName);

    LoginUser findByLoginName(@Param("loginName") String loginName);

}
