package com.sk.tutorialService.util.Mapper;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonMapperBuilder {
    private static ObjectMapper objectMapper = new ObjectMapper();

    public static ObjectMapper getInstance() {
        return objectMapper;
    }

}
