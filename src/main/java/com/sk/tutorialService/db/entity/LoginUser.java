package com.sk.tutorialService.db.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@Table(name = "LOGIN_USER", catalog = "PUBLIC")
public class LoginUser extends CommonEntity {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;

        @Column(name = "DISPLAY_NAME")
        private String displayName;

        @Column(name = "LOGIN_NAME")
        private String loginName;

        @Column(name = "LOGIN_PASS")
        private String loginPass;

        @Column(name = "AUTHORITY")
        private String authority;

        @Column(name = "ENABLED")
        private boolean enabled;


}
