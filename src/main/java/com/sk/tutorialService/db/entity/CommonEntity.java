package com.sk.tutorialService.db.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@MappedSuperclass
public class CommonEntity implements Serializable {


    @Column(name = "INSERT_TIME")
    private Date insertTime;

    @Column(name = "INSERT_USER")
    private String insertUser;

    @Column(name = "INSERT_SYSTEM")
    private String insertSystem;

    @Column(name = "UPDATE_TIME")
    private Date updateTime;

    @Column(name = "UPDATE_USER")
    private String updateUser;

    @Column(name = "UPDATE_SYSTEM")
    private String updateSystem;

    @Column(name = "IS_DELETED")
    private boolean isDeleted;

    @PrePersist
    public void onPrePersist() {
        insertTime = new Date();
        updateTime = new Date();
    }

    @PreUpdate
    public void onPreUpdate() {
        updateTime = new Date();
    }

}
