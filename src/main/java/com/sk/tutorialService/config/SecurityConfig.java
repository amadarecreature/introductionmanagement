package com.sk.tutorialService.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

/**
 * Spring Security設定クラス.
 */
@Slf4j
@Configuration
@EnableWebSecurity // Spring Securityの基本設定
public class SecurityConfig extends WebSecurityConfigurerAdapter {

//        @Autowired
        //        private LoginUserDetailsService loginUserDetailsService;

        @Override
        public void configure(WebSecurity web) {
                // セキュリティ設定を無視するリクエスト設定
                // 静的リソース(images、css、javascript)に対するアクセスはセキュリティ設定を無視する
                web.ignoring().antMatchers("/assets/**");
        }


        @Override
        protected void configure(HttpSecurity http) {
                try {
                        // ログインが必要になったら有効する。
//                        setAuthorization(http);
                        //
                         http.csrf().disable();
                } catch (Exception e) {
                        e.printStackTrace();
                }

        }

        private void setAuthorization(HttpSecurity http) throws Exception {
                // 認可の設定
                http.authorizeRequests().antMatchers("/login/").permitAll() // indexは全ユーザーアクセス許可
                        .anyRequest().authenticated().and().csrf()
                        .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()); // それ以外は全て認証無しの場合アクセス不許可

                http.formLogin().loginPage("/login/").failureUrl("/login/?error").loginProcessingUrl("/login/process/")
                        //                        .loginProcessingUrl("/login/")
                        .defaultSuccessUrl("/schedule/") // 認証成功時の遷移先
                        .usernameParameter("username").passwordParameter("password") // ユーザー名、パスワードのパラメータ名
                        .permitAll().and().rememberMe().useSecureCookie(true);

                // ログアウト設定
                http.logout().logoutUrl("/login/doLogout") // ログアウト処理のパス
                        .logoutSuccessUrl("/login/").permitAll().deleteCookies("JSESSIONID").invalidateHttpSession(true);
                // ログアウト完了時のパス

                http.authorizeRequests().antMatchers("/assets/**").permitAll().anyRequest().authenticated();

        }

        @Bean
        public PasswordEncoder passwordEncoder() {
                return new BCryptPasswordEncoder();
        }
}
