package com.sk.tutorialService.db.code;

import lombok.Getter;

public enum EnumAuthorityRank {
        A("決定権あり"), B("決定権一部あり"), C("決定権なし");

        @Getter
        private String display;

        EnumAuthorityRank(String display) {

                this.display = display;
        }

}
