package com.sk.tutorialService.service;

import com.sk.tutorialService.db.entity.CodeManage;
import com.sk.tutorialService.db.repository.CodeManageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CodeManageService {

    private CodeManageRepository codeManageRepository;

    public CodeManageService(@Autowired CodeManageRepository codeManageRepository) {
        this.codeManageRepository = codeManageRepository;
    }

    public String getCustomerCodeNext() {

        String codeName = "customerCode";
        return getCode(codeName, "CU");
    }

    public String getCustomerPersonNext() {

        String codeName = "customerPersonCode";
        return getCode(codeName, "CUP");
    }

    public String getCustomerOfficeNext() {

        String codeName = "customerOfficeCode";
        return getCode(codeName, "CUF");
    }

    public String getSalesStaffNext() {

        String codeName = "salesStaff";
        return getCode(codeName, "SSF");
    }

    public String getLoginUserNext() {

        String codeName = "login";
        return getCode(codeName, "L");
    }

    public String getProductNext() {

        String codeName = "product";
        return getCode(codeName, "PD");
    }

    private String getCode(String codeName, String prefix) {
        CodeManage codeManage = codeManageRepository.findByCodeName(codeName);
        if (codeManage == null) {
            codeManage = new CodeManage();
            codeManage.setCodeName(codeName);
            codeManage.setCodePrefix(prefix);
            codeManage.setCurrentCodeNumber00(1000L);
            codeManage.setCurrentCodeNumber01(10L);
            codeManage.setIncrement_00(10L);
            codeManage.setIncrement_01(10L);
        } else {
            long next00 = codeManage.getCurrentCodeNumber00() + codeManage.getIncrement_00();
            long next01 = codeManage.getCurrentCodeNumber01() + codeManage.getIncrement_01();
            codeManage.setCurrentCodeNumber00(next00);
            codeManage.setCurrentCodeNumber01(next01);
        }

        codeManageRepository.save(codeManage);

        String result =
                codeManage.getCodePrefix() + String.format("%07d", codeManage.getCurrentCodeNumber00()) + "-" + String
                        .format("%04d", codeManage.getCurrentCodeNumber01());

        return result;
    }
}
