package com.sk.tutorialService.db.repository;

import com.sk.tutorialService.db.entity.CodeManage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface CodeManageRepository extends JpaRepository<CodeManage, Long> {

        CodeManage findByCodeName(@Param("codeName") String codeName);

}
