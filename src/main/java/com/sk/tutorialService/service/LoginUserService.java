package com.sk.tutorialService.service;

import com.sk.tutorialService.db.entity.LoginUser;
import com.sk.tutorialService.db.repository.LoginUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginUserService {

    private LoginUserRepository loginUserRepository;

    public LoginUserService(@Autowired LoginUserRepository loginUserRepository) {
        this.loginUserRepository = loginUserRepository;
    }

    /**
     * パスワードをエンコードして登録する。
     *
     * @param loginUser
     * @return
     */
    public LoginUser save(LoginUser loginUser) {
        LoginUser saveResult = loginUserRepository.save(loginUser);
        return saveResult;
    }

    public void deleteAll() {
        loginUserRepository.deleteAll();
    }

}
