package com.sk.tutorialService.service;

import com.sk.tutorialService.db.entity.TutorialBasis;
import com.sk.tutorialService.db.repository.TutorialBasisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TutorialBasisService {

        private final TutorialBasisRepository tutorialBasisRepository;

        public TutorialBasisService(@Autowired TutorialBasisRepository tutorialBasisRepository) {
                this.tutorialBasisRepository = tutorialBasisRepository;
        }

        public List<TutorialBasis> findAll() {
                return tutorialBasisRepository.findAll();
        }
}
