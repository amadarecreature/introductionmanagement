package com.sk.tutorialService.annotation;

import org.springframework.security.core.annotation.AuthenticationPrincipal;

import java.lang.annotation.*;

/**
 * Spring Securityでログイン時に設定したセッション情報を取得するためのアノテーション
 * セッションに設定された、LoginSessionDataを設定する。
 */
@Target({ ElementType.PARAMETER,ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@AuthenticationPrincipal(expression = "loginSessionData")
public @interface LoginSession {}
