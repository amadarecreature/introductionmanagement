package com.sk.tutorialService.db.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@Table(name = "TUTORIAL_BASIS", catalog = "PUBLIC")
public class TutorialBasis extends CommonEntity {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;


        @Column(name = "TUTORIAL_CODE")
        private String tutorialCode;


        @Column(name = "TITLE")
        private String title;



}
