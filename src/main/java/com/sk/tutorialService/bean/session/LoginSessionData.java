package com.sk.tutorialService.bean.session;

import lombok.Data;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import java.io.Serializable;

@Data
@Component
@SessionScope
public class LoginSessionData implements Serializable {

        private String loginId;

}
