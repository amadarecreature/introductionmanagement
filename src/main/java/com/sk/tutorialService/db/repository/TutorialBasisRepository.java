package com.sk.tutorialService.db.repository;

import com.sk.tutorialService.db.entity.TutorialBasis;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TutorialBasisRepository extends JpaRepository<TutorialBasis,Long> {

}
